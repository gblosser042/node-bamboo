/**
 * Created by gblosser on 4/10/14.
 */
module.exports = function (url, type, callback) {
	"use strict";
	var http = require('http');
	if (url && type) {
		http.get(url, function (res) {
			var resp = {
				"status": res.statusCode,
				"data": ""
			};
			res.on('data', function (chunk) {
				resp.data += chunk.toString();
			});
			res.on('end', function () {
				if (resp.data.substring(0, 5) === 'Error') {
					resp.status = 404;
				}
				callback(undefined, resp);
			});
		}).on('error', function (err) {
			console.log("Error: " + err.message);
			callback(err);
		});
	}
	else {
		callback("Either URL or Type is not set");
	}
};