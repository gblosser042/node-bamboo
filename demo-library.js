"use strict";
var assert = require('assert');
var server = require('./server.js');
var integration = require('./integration.js');
var English = require('yadda').localisation.English;
var ping = require('./ping.js');
var fs = require('fs');
var sel = require('./selenium.js');
var client4000 = require('./client4000.js');

module.exports = (function () {
	var url, type;
	afterEach(function () {
		if (this.currentTest.state !== "passed") {
			var finalMessage = this.currentTest.title + ":" + this.currentTest.err.name + "-" + this.currentTest.err.message;
			fs.appendFile('./email.txt', finalMessage + '\n');
		}
	});
	return English.library()
		.given("$LEVEL event is set up", function (level, next) {
			if (!shouldContinue(level)) {
				next();
			}
			else switch (level) {
				case "client":
					assert.equal(ping('ping'), "pong");
					next();
					break;
				case "server":
					next();
					break;
				case "integration":
					next();
					break;
			}
		})
		.when("$LEVEL action happen", function (level, next) {
			if (!shouldContinue(level)) {
				next();
			}
			else switch (level) {
				case "client":
					assert.equal(ping('beep'), "boop");
					next();
					break;
				case "server":
					var port = server();
					serverget("http://localhost:" + port, function (data) {
						assert.equal(data, "Hai Thar!");
						next();
					});
					break;
				case "integration":
					next();
					break;
			}
		})
		.then("$LEVEL response is recorded", function (level, next) {
			if (!shouldContinue(level)) {
				next();
			}
			else switch (level) {
				case "client":
					assert.equal(ping('cling'), "clong");
					next();
					break;
				case "server":
					var port = server();
					serverget("http://localhost:" + port, function (data) {
						assert.equal(data, "Hai Thar!");
						next();
					});
					break;
				case "integration":
					sel(undefined, 1, function (err) {
						assert.equal(undefined, err);
						next();
					});
					break;
			}
		})
		.given("Closing Time", function (next) {
			next();
		})
		.when("Closing Time", function (next) {
			next();
		})
		.then("Closing Time", function (next) {
			next();
		})
		.given("Viewability is enabled", function (next) {
			next();
		})
		.when("The formatter is the $FORMAT formatter", function (format, next) {
			url = "http://mock-nessy-message.dotomi.com:3000/" + format;
			type = format;
			next();
		})
		.then("Viewability should load correctly", function (next) {
			client4000(url, type, function (err, resp) {
				if (!err) {
					assert.equal(resp.status, "200");
					url = "";
					type = "";
					next();
				}
				else {
					assert.equal("", err);
					url = "";
					type = "";
					next();
				}
			});
		});
})();

function serverget(url, cb) {
	var http = require('http');
	var stream = require('stream');
	http.get(url, function callback(response) {
		response.on("data", function (data) {
			var str = data.toString();
			cb(str);
		});
	});
}

function shouldContinue(level) {
	var message = fs.readFileSync('./email.txt').toString().trim();
	return message === "" || message.toLowerCase().indexOf(level.toLowerCase(), 0) > -1;
}
