Feature: Demo

@Client
Scenario: A Client Test
   
   Given client event is set up
   When client action happen
   Then client response is recorded

@Server
Scenario: A Server Test
   
   Given server event is set up
   When server action happen
   Then server response is recorded

@Integration
Scenario: An Integration Test

   Given integration event is set up
   When integration action happen 
   Then integration response is recorded

@Close
Scenario: Closing

   Given Closing Time

@Client
Scenario: Ad is rendered on a supported environment in flash format

  Given Viewability is enabled.
  When The formatter is the flash formatter
  Then Viewability should load correctly

@Client
Scenario: Ad is rendered on a supported environment in JS format

  Given Viewability is enabled.
  When The formatter is the js formatter
  Then Viewability should load correctly

@Client
Scenario: Ad is rendered on a supported environment in html format

  Given Viewability is enabled.
  When The formatter is the html formatter
  Then Viewability should load correctly