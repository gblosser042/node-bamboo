module.exports = function (search, callback) {
	search = search.toLowerCase()
	var webdriver = require('selenium-webdriver');
	var driver = new webdriver.Builder().withCapabilities(webdriver.Capabilities.chrome()).build();
	driver.get('http://www.google.com/');

	driver.findElement(webdriver.By.name('q')).sendKeys(search);
	//element.submit();
	driver.findElement(webdriver.By.name('btnG')).click();

	driver.wait(function () {
		return driver.getTitle().then(function (title) {
			return title.toLowerCase().lastIndexOf(search, 0) === 0;
		});
	}, 5000);

	driver.getTitle().then(function (title) {
		driver.close();
		callback(title)
	});

	driver.quit();
}
