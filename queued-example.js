var q = require('q'),
	async = require('async'),
	runningProcessCount = 0,
	processQueuePosition = 1,
	processLimit = 2,
	totalProcesses = 10;

function throttleProcesses(){
	if(processLimit > runningProcessCount){
		fakeProcess();
		return true;
	} else {
		return false;
	}
}

function fakeProcess(){
	var timeout = 1000 * (processQueuePosition);
	console.log(processQueuePosition+' is currently running.');
	runningProcessCount++;
	var thisProcessPosition = processQueuePosition;
	processQueuePosition++;
	setTimeout(function(){
		processFinished(thisProcessPosition);
	},timeout);
}

function processFinished(processNumber){
	--runningProcessCount;
	console.log(processNumber+' is finished being run.');
	if(processQueuePosition <= totalProcesses){
		throttleProcesses();
	}
}
	
while(processQueuePosition <= totalProcesses){
	console.log('starting '+processQueuePosition+' of '+totalProcesses);
	if(!throttleProcesses()){
		break; // we hit the queue limit, so bounce from this init loop
	}
}