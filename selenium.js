'use strict';
module.exports = function (test, number, callback) {
	var soda = require('soda'),
		assert = require('assert'),
		brows = require('./browsers.js'),
		async = require('async');

	if (test === undefined) {
		test = function (browser, ending) {
			browser
				.session()
				.open('/test/guinea-pig')
				.getTitle(function (title) {
					assert.ok(title.indexOf('I am a page title - Sauce Labs') > -1);
				}).end(ending);
		};
	}

	brows(function (err, browsers) {
		var errors = "";
		if (err) {
			errors = err.toString();
		}
		var complete = 0;
		var duplicates = 0;
		var original = 0;
		var capacity = 30;
		var mac = 0;
		var actual = 0;
		var fin = {};
		var expected = number;
		if (expected === 0) {
			expected = browsers.length - 1;
		}
		console.log("Processing " + expected + " browsers                        ");
		function check(t, cb) {
			var b = {};
			b.url = 'http://saucelabs.com/';
			b.username = 'gblosser';
			b['access-key'] = 'a52e9396-7c34-42df-a2d6-56de848fc048';
			b.name = 'Prototype Tests';
			b.os = t.os;
			if (t.long_name.toLowerCase() !== 'internet explorer') {
				b.browser = t.long_name.replace(/ /g, '');
			}
			else {
				b.browser = t.long_name;
			}
			b['browser-version'] = t.short_version;
			var perc = Math.round((complete / expected) * 100);
			if (perc < 10) {
				perc = "0" + perc;
			}
			write(perc + "% | Actual:" + actual + " | Mac:" + mac + " | Duplicate: " + duplicates + " | Original:" + original);
			if (fin[stringy(b)] !== "Processed") {
				fin[stringy(b)] = "Processed";
				original = original + 1;
			}
			else {
				duplicates = duplicates + 1;
				complete = complete + 1;
				return cb();
			}

			var browser = soda.createSauceClient(b);
			browser
				.chain
				.and(function (browser) {
					test(browser, function (err) {
						this.setContext('sauce:job-info={"passed": ' + (err === null) + '}', function () {
							browser.testComplete(function () {
								if (err) {
									if (b.browser !== "Safari" || b['browser-version'].toString() !== "5") {
										errors = errors + " " + stringy(b);
									}
								}
								complete = complete + 1;
								if (complete > expected) {
									complete = expected;
								}
								actual = actual + 1;
								perc = Math.round((complete / expected) * 100);
								if (perc < 10) {
									perc = "0" + perc;
								}
								write(perc + "% | Actual:" + actual + " | Mac:" + mac + " | Duplicate: " + duplicates + " | Original:" + original);
								return cb();
							});
						});
					});
				});
		}

		var queue = async.queue(check, capacity);
		browsers.forEach(function (value, index) {
			if (index < expected) {
				queue.push(value);
			}
		});
		queue.drain = comp;
		function comp() {
			write("                                                         ");
			if (errors === "") {
				errors = undefined;
			}
			callback(errors);
		}
	});
};

function stringy(b) {
	return b.browser + ":" + b['browser-version'] + " - " + b.os;
}

function write(mess) {
	process.stdout.write(mess + '\r');
}