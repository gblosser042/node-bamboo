var Yadda = require('yadda');
var fs = require('fs');
Yadda.plugins.mocha.AsyncStepLevelPlugin.init();

new Yadda.FeatureFileSearch('features').each(function (file) {

	featureFile(file, function (feature) {

		var library = require('../demo-library');
		var yadda = new Yadda.Yadda(library);

		scenarios(feature.scenarios, function (scenario) {
			if (scenario.annotations['server'])
				steps(scenario.steps, function (step, done) {
					yadda.yadda(step, done);
				});
		});
		scenarios(feature.scenarios, function (scenario) {
			if (scenario.annotations['client'])
				steps(scenario.steps, function (step, done) {
					yadda.yadda(step, done);
				});
		});
		scenarios(feature.scenarios, function (scenario) {
			if (scenario.annotations['integration'])
				steps(scenario.steps, function (step, done) {
					yadda.yadda(step, done);
				});
		});
		scenarios(feature.scenarios, function (scenario) {
			if (scenario.annotations['close'])
				steps(scenario.steps, function (step, done) {
					email(function (error) {
						yadda.yadda(step, done);
					})
				});
		});
	});
});
function email(cb) {
	var nodemailer = require("nodemailer");
	var target = 'gblosser@conversantmedia.com';
	var message = fs.readFileSync('./email.txt').toString();
	if (message.trim() == "") {
		fs.truncateSync('./email.txt');
		cb();
	}

	var smtpTransport = nodemailer.createTransport("SMTP",
		{
			service: "Gmail",
			auth: {
				user: "gblosser42@gmail.com",
				pass: "w@tiip42"
			}
		});

	var mailOptions = {
		from: "Mocha Alerts <Mocha@dotomi.com>",
		to: target,
		subject: "Failed Test",
		text: message,
		html: "<b>" + message.replace(/\n/g, '<p/>') + "</b>"
	};

	smtpTransport.sendMail(mailOptions, function (error, response) {
		if (error) {
			console.log(error);
			cb(error)
		} else {
			cb()
		}
	});
	fs.truncateSync('./email.txt', 0)
}
